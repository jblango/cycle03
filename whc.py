import sys
import urllib
from http.cookiejar import CookieJar
from urllib.request import build_opener, HTTPCookieProcessor,Request,urlopen

def getHTTPHeader(url):
    filenameSaved, httpHeaders = urllib.request.urlretrieve(url)
    html = open(filenameSaved)
    print(filenameSaved)
    print(httpHeaders)

    
def getCookieInformation(url):
    cookieJar = CookieJar()
    buildOpenerCookie = build_opener(HTTPCookieProcessor(cookieJar))
    httpClient = buildOpenerCookie.open(url)
    
    cookies = list(cookieJar)
    print('')
    print(cookies)
    print('')
    print('---------------key value display---------------------')
    for cookie in cookies:
        print('Name: ', cookie.name)
        print('Value: ', cookie.value)
        print('Domain initial dot: ',cookie.domain_initial_dot)
        print('Domain: ',cookie.domain)
        print('Expires: ',cookie.expires)
        if ('httponly' in cookie._rest.keys()):
            print('Httponly: ', 'True')
        else:
            print('Httponly: ', 'False')
        print('Comment: ', cookie.comment)
  
        if (not cookie.secure):
            cookie.secure = 'NOT secure'
        else:
            print('Secure: ',cookie.secure)
            
def getHTTPHeaderInformation(url):
    req2 = urllib.request.Request(url)
    response = urlopen(req2)
    h1 = response.getheaders()
    print(h1)

def main():
    
    if len(sys.argv) != 2:
        print(">whc.py url")
        sys.exit("Exiting...") 
    url = sys.argv[1]

    print('--------- HTTP Header Method 1 -------')
    getHTTPHeader(url)
    print('')
    print('--------- HTTP Header Method 2 -------')
    getHTTPHeaderInformation(url)
    print('')
    print('--------- Cookies -------')
    getCookieInformation(url)
  
                         
if __name__ == "__main__":
    main()
